let mapleader =","

" Spell-check set to <leader>o, 'o' for 'orthography':
  map <leader>o :setlocal spell! spelllang=en_gb<CR>

" inserting time stamps
  nnoremap <F5> "=strftime("%c")<CR>P
  inoremap <F5> <C-R>=strftime("%c")<CR>

" set no backup swap file
" Have set these up so that we can keep all the backup / swap and undo files in one location.
" the // at the end is so that you get the full path of the file so that you get the full path
  set backupdir=~/.vim/backup//
  set directory=~/.vim/swap//
  set undodir=~/.vim/undo//

" Some basics:
  set textwidth=100
  set mouse=a
  set nohlsearch
  set cursorline
  set nocompatible
  filetype plugin on
  syntax on
  set encoding=utf-8
  set number relativenumber
  map <leader>no :set norelativenumber nu! nocursorline<CR>
  map <leader>on :set number relativenumber<CR>

" Enable autocompletion:
  set wildmode=longest,list,full

" Markdown
  au BufNewFile,BufRead *.markdown,*.mdown,*.mkd,*.mkdn,README.md  setf markdown

" Disables automatic commenting on newline:
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
  set splitbelow splitright

" Check file in shellcheck:
  map <leader>s :!clear && shellcheck %<CR>

" List of plugins which will automatically install the plugins if not there.  
  let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim' 
  if empty(glob(data_dir . '/autoload/plug.vim'))   
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'   
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC 
  endif

  call plug#begin()
    Plug 'ycm-core/YouCompleteMe'
    Plug 'gmarik/Vundle.vim'
    Plug 'airblade/vim-gitgutter'
    Plug 'voldikss/vim-floaterm'
    Plug 'vim-scripts/indentpython.vim'
    Plug 'scrooloose/syntastic'
    Plug 'nvie/vim-flake8'
    Plug 'scrooloose/nerdtree'
    Plug 'jistr/vim-nerdtree-tabs'
    Plug 'tmhedberg/SimpylFold'
    Plug 'davidhalter/jedi-vim'
    Plug 'klen/python-mode'
    Plug 'vimwiki/vimwiki'
    Plug 'mattn/calendar-vim'
    Plug 'jacoborus/tender.vim'
    Plug 'vim-airline/vim-airline'
    Plug 'junegunn/fzf.vim'
    Plug 'junegunn/fzf'
    Plug 'heavenshell/vim-pydocstring', { 'do': 'make install', 'for': 'python' }
  call plug#end()
  filetype plugin indent on

" Nerd tree
  map <C-Y> :NERDTreeToggle<CR>
  let g:NERDTreeDirArrows = 1
  let g:NERDTreeDirArrowExpandable = '▸'
  let g:NERDTreeDirArrowCollapsible = '▾'
  autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
  let g:fzf_preview_window = 'right:50%'
  let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6  }  }

" Enable folding
  nnoremap \z :setlocal foldexpr=(getline(v:lnum)=~@/)?0:(getline(v:lnum-1)=~@/)\\|\\|(getline(v:lnum+1)=~@/)?1:2 foldmethod=expr foldlevel=0 foldcolumn=2<CR>
  set foldmethod=indent
  set foldlevel=99
" Enable folding with the space bar
  nnoremap <space> za

" Python checks. 
  let g:syntastic_python_checkers=['flake8']
  let g:syntastic_always_populate_loc_list = 1
  let g:syntastic_auto_loc_list = 1
  let g:syntastic_check_on_open = 1
  let g:syntastic_check_on_wq = 0

" for yaml checking
  set statusline+=%#warningmsg#
  set statusline+=%{SyntasticStatuslineFlag()}
  set statusline+=%*

" Diary commands:
" ,w,w will create you a new diary entry.
  command! Diary VimwikiDiaryIndex
  augroup vimwikigroup
      autocmd!
      " automatically update links on read diary
      autocmd BufRead,BufNewFile diary.md VimwikiDiaryGenerateLinks
  augroup end

" vimwiki setup 
  let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]

" set tabs
  set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab

" set Smart indent
  set smartindent

" Colour scheme
  set background=dark
  colorscheme iceberg

" python docstring
  let g:pydocstring_doq_path="$HOME/.local/bin/doq"
  let g:pydocstring_formatter = 'numpy'
  nmap <silent> <C-_> <Plug>(pydocstring)

" for airline
  let g:airline_powerline_fonts=1
  let g:airline#extensions#tabline#enabled = 1

"split navigations
  map <leader>" :sp<CR>
  map <leader>% :vsp<CR>
  nnoremap <C-J> <C-W><C-J>
  nnoremap <C-K> <C-W><C-K>
  nnoremap <C-L> <C-W><C-L>
  map <C-H> <C-W><C-H>
" resize panes
  nnoremap <silent><Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
  nnoremap <silent><Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>
  nnoremap <silent><Leader>> :exe "vertical resize " . (winwidth(0) * 3/2)<CR>
  nnoremap <silent><Leader>< :exe "vertical resize " . (winwidth(0) * 2/3)<CR>

" Terminal stuff
  let g:floaterm_keymap_toggle = '<F12>'
  let g:floaterm_width = 0.9
  let g:floaterm_height = 0.9

" You complete me
  call prop_type_add( 'YCM_HL_parameter', { 'highlight': 'Normal' } )
