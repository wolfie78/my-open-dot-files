# This is for bashrc 

# Setting up the aliases
if [ -f ~/.bash_alias ]; then
  . ~/.bash_alias
fi

# Setting up the aliases
if [ -f ~/.bash_path ]; then
  . ~/.bash_path
fi

# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

export TERM=xterm-256color
