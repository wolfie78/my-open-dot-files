# My dot files.
This will install my dot files and put them all in the correct place, the ansible playbook should
only be used with Fedora based OS's though, if you want clone it you can delete the top section fo
the install of software.

## backup and update the following dot files:
| Git file name | Destination                 | Comments                                                                       |
| ----------    | ----------                  | -------------------                                                            |
| bash_alias    | `~/.bash_alias`             | List of alias's                                                                |
| bash_path     | `~/.bash_path`              | This is a list of aliases that I use                                           |
| bash_profile  | `~/.bash_profile`           | My generic bash_profile                                                        |
| bashrc        | `~/.bashrc`                 | This is my bashrc file                                                         |
| gitconfig     | `~/.gitconfig`              | list of shortcuts for git that are useful                                      |
| iceberg.vim   | `~/.vim/colors/iceberg.vim` | this is my color scheme for vim                                                |
| tmux.conf     | `~/.tmux.conf`              | This is my tmux conf uses, ctrl+a, instead of ctrl+b                           |
| vimrc         | `~/.vimrc`                  | This is my default vimrc file, it will also install any plugins automatically. |

## Install the following software
This will install the following software, if you dont want it, when you have cloned it just comment
out the first section, these plugins are needed for `YouCompleteMe` plugin.
| software      | description                                     |
| ------------  | -------------                                   |
| python3-devel | This is used for the `youcompleteme` vim plugin |
| gcc           | This is used for the `youcompleteme` vim plugin |
| gcc-c++       | This is used for the `youcompleteme` vim plugin |
| go            | This is used for the `youcompleteme` vim plugin |
| npm           | This is used for the `youcompleteme` vim plugin |

## How to run:
if you want to install this with ansible you can run:
```
ansible-playbook setup_localhost.yml -K 
```

Afterwards, you might want to run:
```
  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"
```

So that these details are updated in your `.gitconfig`
