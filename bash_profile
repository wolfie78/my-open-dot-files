# To stop shellcheck complaining # Setting up the aliases if [ -f ~/.bash_alias ]; then
if [ -f ~/.bash_alias ]; then
  . ~/.bash_alias
fi
# This is for area specific aliases
if [ -f ~/.bash_wolfie ]; then
  . ~/.bash_wolfie
fi
# Setting up extra paths
if [ -f ~/.bash_path ]; then
  . ~/.bash_path
fi

# Getting the variables
if [ -f /etc/redhat-release ]; then
  Rel=$(cat /etc/redhat-release)
elif [ -f /etc/os-release ]; then
  Rel=$(grep PRETTY_NAME /etc/os-release | cut -f 2 -d =)
fi

# setting up editor
export EDITOR='vimx'

# Getting some variables for the standard greeting.
countCPU=$(grep -c processor < /proc/cpuinfo) 
MEM=$(grep ^MemTotal: /proc/meminfo | awk -F: '{print $2}' | sed -e 's/^[ \t]*//')
USERS=$(who | wc -l)

# Showing the output
echo -e "\n----------------------------------------------------------------------\n"
echo -e "Release Info     ${Rel}"
echo -e "Kernel Info      $(uname -r)"
echo -e "This is BASH     ${BASH_VERSION%.*} - DISPLAY on $DISPLAY${NC}"
echo -e "Current date     $(date)"
echo -e "Number CPU       $countCPU " 
echo -e "Memory Amount    ${MEM} " 
echo -e "Uptime           $(uptime| awk '{print $3" "$4}'| sed 's/,//') "
echo -e "Amount of users  ${USERS}"
echo -e "Load Average     $(uptime | awk '{print $8" "$9" "$10" "$11" "$12}')"
echo -e "\n----------------------------------------------------------------------\n"

# setting up the command prompt to show what branch you are on in git.
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "
